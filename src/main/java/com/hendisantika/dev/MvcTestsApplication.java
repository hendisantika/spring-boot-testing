package com.hendisantika.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by IntelliJ IDEA.
 * Project : mvc-test
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/11/17
 * Time: 05.19
 * To change this template use File | Settings | File Templates.
 */


@SpringBootApplication
public class MvcTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcTestsApplication.class, args);
	}
}

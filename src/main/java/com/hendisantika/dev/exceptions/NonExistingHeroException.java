package com.hendisantika.dev.exceptions;

/**
 * This exception is thrown when the SuperHero can't be found in the application if searching by ID.
 * Created by IntelliJ IDEA.
 * Project : mvc-test
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/11/17
 * Time: 05.25
 * To change this template use File | Settings | File Templates.
 */

public class NonExistingHeroException extends RuntimeException {
}
